package com.zftlive.android.sample.tabs.radiogroup;

import android.os.Bundle;
import android.view.View;

import com.zftlive.android.library.base.ui.CommonActivity;

/**
 * tab主界面实现示例（基于RadioGroup+Fragment+FragmentManager实现，特点就是不能滑动）
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class HomeTabActivity extends CommonActivity {

  @Override
  public int bindLayout() {
    return 0;
  }

  @Override
  public void initParms(Bundle parms) {

  }

  @Override
  public void initView(View view) {

  }
}
