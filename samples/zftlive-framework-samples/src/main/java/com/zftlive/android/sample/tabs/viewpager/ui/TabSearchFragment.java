package com.zftlive.android.sample.tabs.viewpager.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.zftlive.android.R;
import com.zftlive.android.library.base.ui.BaseFragmentV4;

/**
 * [搜索]tab界面
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class TabSearchFragment extends BaseFragmentV4 {

  TextView mText;
  
  @Override
  public int bindLayout() {
    return R.layout.fragment_tabs_fav;
  }

  @Override
  public void initParams(Bundle params) {

  }

  @Override
  public void initView(View view) {
    mText = (TextView) findViewById(R.id.text);
  }

  @Override
  public void doBusiness(Context mContext) {

  }
  
  @Override
  public void loadDataOnce() {
    super.loadDataOnce();
//    mText.setText("搜索tab界面");
  }

}
