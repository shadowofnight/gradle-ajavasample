package com.zftlive.android.sample.tabs.viewpager.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentManager;

import com.zftlive.android.library.common.adapter.BasicFragmentPagerAdapter;

/**
 * 用于固定数量tab的标签页情况
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class FragmentTabAdapter extends BasicFragmentPagerAdapter {

  public FragmentTabAdapter(FragmentManager fm, Activity mActivity) {
    super(fm, mActivity);
  }

}
