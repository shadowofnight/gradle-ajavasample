package com.zftlive.android.sample.tabs.viewpager.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RadioButton;

import com.zftlive.android.R;
import com.zftlive.android.library.base.ui.CommonActivity;
import com.zftlive.android.sample.tabs.viewpager.adapter.FragmentTabAdapter;
import com.zftlive.android.sample.tabs.viewpager.bean.TabBean;

/**
 * tab主界面实现示例（基于ViewPager+Fragment实现，支持滑动与点击切换，主流实现方式）
 * 
 * @author 曾繁添
 * @version 1.0
 * 
 */
public class MainVPTabActivity extends CommonActivity
    implements
      View.OnClickListener,
      ViewPager.OnPageChangeListener {

  /**
   * Tab容器ViewPager
   */
  private ViewPager mTabViewPager;

  /**
   * tab适配器
   */
  private FragmentTabAdapter mTabAdapter;

  /**
   * tab切换按钮
   */
  private RadioButton mHomeRb, mFavrateRb, mMessageRb, mSearchRb, mSettingRb;

  @Override
  public int bindLayout() {
    return R.layout.activity_tabs_viewpager;
  }

  @Override
  public void initParms(Bundle parms) {

  }

  @Override
  public void initView(View view) {
    // tab切换按钮
    mHomeRb = (RadioButton) findViewById(R.id.rb_home);
    mHomeRb.setOnClickListener(this);
    mFavrateRb = (RadioButton) findViewById(R.id.rb_favrate);
    mFavrateRb.setOnClickListener(this);
    mMessageRb = (RadioButton) findViewById(R.id.rb_message);
    mMessageRb.setOnClickListener(this);
    mSearchRb = (RadioButton) findViewById(R.id.rb_search);
    mSearchRb.setOnClickListener(this);
    mSettingRb = (RadioButton) findViewById(R.id.rb_setting);
    mSettingRb.setOnClickListener(this);

    mTabViewPager = (ViewPager) findViewById(R.id.vp_tabs);
    mTabAdapter = new FragmentTabAdapter(getSupportFragmentManager(), getContext());
  }

  @Override
  public void doBusiness(Context mContext) {
    // 初始化标题栏
    initHomeMenuTitleBar("主界面", null);
    hiddenLeftMenuBtn(View.GONE);

    // 构建tab数据
    mTabAdapter.addItem(new TabBean(TabHomeFragment.class));
    mTabAdapter.addItem(new TabBean(TabFavrateFragment.class));
    mTabAdapter.addItem(new TabBean(TabMessageFragment.class));
    mTabAdapter.addItem(new TabBean(TabSearchFragment.class));
    mTabAdapter.addItem(new TabBean(TabSettingFragment.class));
    mTabViewPager.setAdapter(mTabAdapter);
    mTabAdapter.notifyDataSetChanged();
    //当前tab两边各一个，内存中保留三个，默认设置
    mTabViewPager.setOffscreenPageLimit(1);
    mTabViewPager.setCurrentItem(0);
    mTabViewPager.setOnPageChangeListener(this);
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {
    // 滑动设置标题、RB选中
    // CharSequence strTitle = mTabAdapter.getPageTitle(position);
    // setWindowTitle(strTitle, Gravity.CENTER);

    //同步RB的状态
    switch (position) {
      case 0:
        mHomeRb.setChecked(true);
        break;
      case 1:
        mFavrateRb.setChecked(true);
        break;
      case 2:
        mMessageRb.setChecked(true);
        break;
      case 3:
        mSearchRb.setChecked(true);
        break; 
      case 4:
        mSettingRb.setChecked(true);
        break;         
      default:
        break;
    }
  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
    // [首页]按钮
      case R.id.rb_home:
        mTabViewPager.setCurrentItem(0);
        break;
      // [收藏]按钮
      case R.id.rb_favrate:
        mTabViewPager.setCurrentItem(1);
        break;
      // [消息]按钮
      case R.id.rb_message:
        mTabViewPager.setCurrentItem(2);
        break;
      // [搜索]按钮
      case R.id.rb_search:
        mTabViewPager.setCurrentItem(3);
        break;
      // [设置]按钮
      case R.id.rb_setting:
        mTabViewPager.setCurrentItem(4);
        break;
      default:
        break;
    }
  }

}
