package com.zftlive.android.sample.basic;

import android.os.Bundle;
import android.view.View;

import com.zftlive.android.library.base.ui.CommonActivity;

/**
 * 联系人选择界面
 * @author 曾繁添
 * @version 1.0
 *
 */
public class ChoicePhoneActivity extends CommonActivity {

	@Override
	public int bindLayout() {
		return 0;
	}

	@Override
	public void initParms(Bundle parms) {

	}

	@Override
	public void initView(View view) {

	}
}
