package com.zftlive.android.sample.tabs.tabhost;

import android.os.Bundle;
import android.view.View;

import com.zftlive.android.library.base.ui.CommonActivity;

/**
 * tab主界面实现示例（基于TabHost+TabActivity实现，该方案基本已被废弃）
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class HomeTabActivity extends CommonActivity {

  @Override
  public int bindLayout() {
    return 0;
  }

  @Override
  public void initParms(Bundle parms) {

  }

  @Override
  public void initView(View view) {

  }

}
