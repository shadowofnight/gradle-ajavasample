package com.zftlive.android.sample.tabs.viewpager.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * 用于数量不定tab的标签页情况，适合很多tab
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class FragmentTabStateAdapter extends FragmentStatePagerAdapter {

  public FragmentTabStateAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int position) {
    return null;
  }

  @Override
  public int getCount() {
    return 0;
  }

}
