package com.zftlive.android.sample.tabs.viewpager.bean;

import android.os.Bundle;

/**
 * Tab标签信息实体Bean
 * 
 * @author 曾繁添
 * @version 1.0
 * 
 */
public final class TabBean extends com.zftlive.android.library.base.bean.TabBean {

  /**
   * 
   */
  private static final long serialVersionUID = -4735217706594914494L;

  public TabBean(Class<?> clss) {
    super("", clss);
  }

  public TabBean(String label, Class<?> clss) {
    super(label, clss);
  }

  public TabBean(String label, Class<?> clss, Bundle args) {
    super(label, clss, args);
  }
}
