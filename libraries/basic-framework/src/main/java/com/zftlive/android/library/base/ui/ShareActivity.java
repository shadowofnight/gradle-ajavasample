package com.zftlive.android.library.base.ui;

import android.view.View;

import com.zftlive.android.library.tools.sharesdk.SharePlatformActionListener;
import com.zftlive.android.library.tools.sharesdk.ShareSDKHelper;

import java.util.ArrayList;
import java.util.HashMap;

import cn.sharesdk.framework.Platform;

/**
 * 原生带分享功能的Activity
 *
 * @author 曾繁添
 * @version 1.0
 */
public abstract class ShareActivity extends BaseActivity {

    /**
     * 分享辅助工具类
     */
    protected ShareSDKHelper mShareSDKHelper;

    @Override
    public void initView(View view) {
        mShareSDKHelper = ShareSDKHelper.getInstance();
        mShareSDKHelper.initSDK(this);
        mShareSDKHelper.setShareListener(new BasicShareListener());
    }

    @Override
    public void destroy() {
        super.destroy();
        if (null != mShareSDKHelper) {
            mShareSDKHelper.stopSDK();
        }
    }

    /**
     * 打开分享对话框
     *
     * @param imageURL   分享图片地址
     * @param title      分享标题
     * @param strContent 分享内容
     * @param linkURL    点击打开的链接地址
     */
    protected void openSharePannel(String imageURL, String title, String strContent, String linkURL) {
        if (null == mShareSDKHelper) return;
        mShareSDKHelper.openSharePannel(this, imageURL, title, strContent, linkURL);
    }

    /**
     * 打开分享对话框
     *
     * @param imageURL      分享图片地址
     * @param title         分享标题
     * @param strContent    分享内容
     * @param linkURL       点击打开的链接地址
     * @param sharePlatform 分享平台,见IBaseConstant.PLATFORM_前缀对应的平台枚举，默认全平台
     */
    public void openSharePannel(String imageURL, String title, String strContent, String linkURL, ArrayList<String> sharePlatform) {
        if (null == mShareSDKHelper) return;
        mShareSDKHelper.openSharePannel(this, imageURL, title, strContent, linkURL, sharePlatform);
    }

    /**
     * 打开分享对话框
     *
     * @param imageResId    分享图片地址，工程本地地址
     * @param title         分享标题
     * @param strContent    分享内容
     * @param linkURL       点击打开的链接地址
     * @param sharePlatform 分享平台,见IBaseConstant.PLATFORM_前缀对应的平台枚举，默认全平台
     */
    public void openSharePannel(int imageResId, String title, String strContent, String linkURL, ArrayList<String> sharePlatform) {
        if (null == mShareSDKHelper) return;
        mShareSDKHelper.openSharePannel(this, imageResId, title, strContent, linkURL, sharePlatform);
    }

    /**
     * 分享面板点击回调
     *
     * @param platform 点击分享平台
     */
    protected void onShareItemClick(Platform platform) {

    }

    /**
     * 分享成功回调
     *
     * @param platform 分享平台
     * @param code     返回状态码
     * @param hashMap  额外参数
     */
    protected void onShareSuccess(Platform platform, int code, HashMap<String, Object> hashMap) {

    }

    /**
     * 分享失败回调
     *
     * @param platform  分享平台
     * @param code      返回状态码
     * @param throwable 异常
     */
    protected void onShareFailure(Platform platform, int code, Throwable throwable) {

    }

    /**
     * 取消分享回调
     *
     * @param platform 分享平台
     * @param code     返回状态码
     */
    protected void onShareCancel(Platform platform, int code) {

    }

    /**
     * 分享回调监听器
     */
    protected class BasicShareListener extends SharePlatformActionListener {

        @Override
        public void onItemClick(Platform platform) {
            onShareItemClick(platform);
        }

        @Override
        public void onSuccess(Platform platform, int i, HashMap<String, Object> hashMap) {
            onShareSuccess(platform, i, hashMap);
        }

        @Override
        public void onFailure(Platform platform, int i, Throwable throwable) {
            onShareFailure(platform, i, throwable);
        }

        @Override
        public void onShareCancel(Platform platform, int i) {
            ShareActivity.this.onShareCancel(platform, i);
        }
    }
}
