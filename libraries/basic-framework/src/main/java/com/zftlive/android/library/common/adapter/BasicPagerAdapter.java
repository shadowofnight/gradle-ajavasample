package com.zftlive.android.library.common.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于自定义View的滑动ViewPager适配器
 *
 * @author 曾繁添
 * @version 1.0
 *
 */
public class BasicPagerAdapter extends PagerAdapter {

    private List<View> mViewList = new ArrayList<View>();
    /**日志输出标志**/
    protected final String TAG = this.getClass().getSimpleName();

    @Override
    public int getCount() {
        return mViewList.size();
    }

    @Override
    public Object instantiateItem(View container, int position) {
        // 初始化标签页
        ((ViewPager) container).addView(mViewList.get(position), 0);
        return mViewList.get(position);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        // 移除标签页
        ((ViewPager) container).removeView(mViewList.get(position));
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        // 用于判断是否需要添加标签页
        return arg0 == arg1;
    }

    /**
     * 获取指定位置的Tab
     * @param position
     * @return
     */
    public View getView(int position){
        if(position < mViewList.size()){
            return mViewList.get(position);
        }
        return null;
    }

    /**
     * 添加View标签页
     *
     * @param item
     *            View标签页
     */
    public void addViewItem(View item) {
        mViewList.add(item);
    }

    /**
     * 移除View视图item
     * @param item
     */
    public void removeViewItem(View item){
        mViewList.remove(item);
    }

    /**
     * 移除View视图item
     * @param index 位置
     */
    public void removeViewItem(int index){
        mViewList.remove(index);
    }

    /**
     * 移除全部View视图item
     */
    public void clearViewItem(){
        mViewList.clear();
    }
}
