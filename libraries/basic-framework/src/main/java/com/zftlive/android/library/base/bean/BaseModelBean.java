package com.zftlive.android.library.base.bean;

/**
 * 基础数据模型Bean
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class BaseModelBean extends BaseBean{

  /**
   * 
   */
  private static final long serialVersionUID = -2268555463658186721L;

  /**
   * item类型
   */
  public int itemType = 0;

}
