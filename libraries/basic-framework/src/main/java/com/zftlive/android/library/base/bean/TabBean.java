package com.zftlive.android.library.base.bean;

import android.os.Bundle;

/**
 * Fragment类型的Tab数据模型
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class TabBean extends BaseBean {
  
  private static final long serialVersionUID = -814914448481197650L;

  /**
   * tab显示文本
   */
  public String label = "默认";

  /**
   * tab对应的id
   */
  public String value = "999";

  /**
   * 对应Fragment
   */
  public Class<?> clss;
  
  /**
   * Fragment对应参数
   */
  public Bundle args;

  public TabBean(String label, Class<?> clss) {
    this(label,clss,null);
  }

  public TabBean(String label, Class<?> clss, Bundle args) {
    this(label,label,clss,args);
  }

  public TabBean(String label, String value, Class<?> clss, Bundle args) {
    this.label = label;
    this.value = value;
    this.clss = clss;
    this.args = args;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Class<?> getClss() {
    return clss;
  }

  public void setClss(Class<?> clss) {
    this.clss = clss;
  }

  public Bundle getArgs() {
    return args;
  }

  public void setArgs(Bundle args) {
    this.args = args;
  }
}
