package com.zftlive.android.library.tools.sharesdk;


import cn.sharesdk.framework.authorize.AuthorizeAdapter;

/**
 * 授权页面自定义
 */
public class MyAuthorizeAdapter extends AuthorizeAdapter {

    public void onCreate() {
        // 隐藏标题栏右部的ShareSDK Logo
        hideShareSDKLogo();

//		TitleLayout llTitle = getTitleLayout();
//		llTitle.getTvTitle().setText("xxxx");

//		String platName = getPlatformName();
//		if ("SinaWeibo".equals(platName)
//				|| "TencentWeibo".equals(platName)) {
//			initUi(platName);
//			interceptPlatformActionListener(platName);
//			return;
//		}
//
//		// 使弹出动画失效，只能在onCreate中调用，否则无法起作用
//		if ("KaiXin".equals(platName)) {
//			disablePopUpAnimation();
//		}
//
//		// 下面的代码演示如何设置自定义的授权页面打开动画
//		if ("Douban".equals(platName)) {
//			stopFinish = true;
//			disablePopUpAnimation();
//			View rv = (View) getBodyView().getParent();
//			TranslateAnimation ta = new TranslateAnimation(
//					Animation.RELATIVE_TO_SELF, -1,
//					Animation.RELATIVE_TO_SELF, 0,
//					Animation.RELATIVE_TO_SELF, 0,
//					Animation.RELATIVE_TO_SELF, 0);
//			ta.setDuration(500);
//			rv.setAnimation(ta);
//		}
    }
}
