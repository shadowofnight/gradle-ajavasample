package com.zftlive.android.library.widget.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;

import com.zftlive.android.library.base.ui.BaseDialog;
import com.zftlive.android.library.tools.ToolResource;

/**
 * 自定义对话框
 * @author 曾繁添
 * @version 1.0
 */
public class LoadingDialog extends BaseDialog {
	
	/**消息TextView*/
	private TextView tvMsg ; 
	
	public LoadingDialog(Activity context, String strMessage) {
		this(context, ToolResource.getStyleId("CustomProgressDialog"),strMessage);
	}

	public LoadingDialog(Activity context, int theme, String strMessage) {
		super(context, theme);
		this.setContentView(ToolResource.getLayoutId("view_progress_dialog"));
		this.getWindow().getAttributes().gravity = Gravity.CENTER;
	    tvMsg = (TextView) this.findViewById(ToolResource.getIdId("tv_msg"));
	    setMessage(strMessage);
	}

	/**
	 * 设置进度条消息
	 * @param strMessage 消息文本
	 */
	public void setMessage(String strMessage){
		if (tvMsg != null) {
			tvMsg.setText(strMessage);
		}
	}
}