package com.zftlive.android.library.tools.sharesdk;

import java.io.Serializable;

/**
 * 分享平台bean
 *
 */
public class PlatformConfig implements Serializable {

    /**
     * 分享平台
     */
    private String mPlatformName = "";

    /**
     * id
     */
    private String mId = "1";


    /**
     * 九宫格排序顺序
     */
    private String mSortId = "1";

    /**
     * QQ、QQ空间平台接入的AppId
     */
    private String mAppId = "";

    /**
     * 分享平台接入的AppKey
     */
    private String mAppKey = "";


    /**
     * 分享平台接入的AppSecret
     */
    private String mAppSecret = "";

    /**
     * 重定向URL
     */
    private String mRedirectUrl = "http://jr.jd.com/";

    /**
     * 是否通过客户端分享
     */
    private boolean mShareByAppClient = true;

    /**
     * 是否绕过审核权限-微信分享，默认false
     */
    private boolean mBypassApproval = false;

    /**
     * 是否有效
     */
    private boolean mEnable = true;

    public PlatformConfig() {

    }

    public PlatformConfig(String mPlatformName,String mId, String mSortId, String mAppId, String mAppKey, String mAppSecret, String mRedirectUrl) {
        this.mPlatformName = mPlatformName;
        this.mId = mId;
        this.mSortId = mSortId;
        this.mAppId = mAppId;
        this.mAppKey = mAppKey;
        this.mAppSecret = mAppSecret;
        this.mRedirectUrl = mRedirectUrl;
    }

    public PlatformConfig(String mPlatformName,String mId, String mSortId, String mAppKey, String mAppSecret, String mRedirectUrl) {
        this.mPlatformName = mPlatformName;
        this.mId = mId;
        this.mSortId = mSortId;
        this.mAppKey = mAppKey;
        this.mAppSecret = mAppSecret;
        this.mRedirectUrl = mRedirectUrl;
    }

    public PlatformConfig(String mPlatformName,String mId, String mSortId, String mAppId, String mAppKey, String mAppSecret, String mRedirectUrl, boolean mShareByAppClient) {
        this(mPlatformName,mId,mSortId,mAppId,mAppKey,mAppSecret,mRedirectUrl,mShareByAppClient,true);
    }

    public PlatformConfig(String mPlatformName,String mId, String mSortId, String mAppId, String mAppKey, String mAppSecret, String mRedirectUrl, boolean mShareByAppClient, boolean mEnable) {
        this(mPlatformName,mId,mSortId,mAppId,mAppKey,mAppSecret,mRedirectUrl,mShareByAppClient,false,mEnable);
    }

    public PlatformConfig(String mPlatformName,String mId, String mSortId, String mAppId, String mAppKey, String mAppSecret, String mRedirectUrl, boolean mShareByAppClient, boolean mBypassApproval, boolean mEnable) {
        this.mPlatformName = mPlatformName;
        this.mId = mId;
        this.mSortId = mSortId;
        this.mAppId = mAppId;
        this.mAppKey = mAppKey;
        this.mAppSecret = mAppSecret;
        this.mRedirectUrl = mRedirectUrl;
        this.mShareByAppClient = mShareByAppClient;
        this.mBypassApproval = mBypassApproval;
        this.mEnable = mEnable;
    }

    public String getmPlatformName() {
        return mPlatformName;
    }

    public void setmPlatformName(String mPlatformName) {
        this.mPlatformName = mPlatformName;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmSortId() {
        return mSortId;
    }

    public void setmSortId(String mSortId) {
        this.mSortId = mSortId;
    }

    public String getmAppId() {
        return mAppId;
    }

    public void setmAppId(String mAppId) {
        this.mAppId = mAppId;
    }

    public String getmAppKey() {
        return mAppKey;
    }

    public void setmAppKey(String mAppKey) {
        this.mAppKey = mAppKey;
    }

    public String getmAppSecret() {
        return mAppSecret;
    }

    public void setmAppSecret(String mAppSecret) {
        this.mAppSecret = mAppSecret;
    }

    public String getmRedirectUrl() {
        return mRedirectUrl;
    }

    public void setmRedirectUrl(String mRedirectUrl) {
        this.mRedirectUrl = mRedirectUrl;
    }

    public boolean ismShareByAppClient() {
        return mShareByAppClient;
    }

    public void setmShareByAppClient(boolean mShareByAppClient) {
        this.mShareByAppClient = mShareByAppClient;
    }

    public boolean ismBypassApproval() {
        return mBypassApproval;
    }

    public void setmBypassApproval(boolean mBypassApproval) {
        this.mBypassApproval = mBypassApproval;
    }

    public boolean ismEnable() {
        return mEnable;
    }

    public void setmEnable(boolean mEnable) {
        this.mEnable = mEnable;
    }
}
