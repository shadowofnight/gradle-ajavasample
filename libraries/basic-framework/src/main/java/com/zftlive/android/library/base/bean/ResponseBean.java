package com.zftlive.android.library.base.bean;

/**
 * HTTP交互响应Bean
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class ResponseBean extends BaseBean {

  /**
   * 
   */
  private static final long serialVersionUID = 6621721372048872692L;

  /**
   * 错误提示
   */
  public String errorMsg;

  /**
   * 状态码
   */
  public int resultCode;

}
