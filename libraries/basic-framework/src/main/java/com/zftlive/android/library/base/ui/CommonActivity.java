package com.zftlive.android.library.base.ui;

import android.content.Context;

/**
 * 一般常用的Activity
 *
 * @author 曾繁添
 * @version 1.0
 *
 */
public abstract class CommonActivity extends BaseActivity {

    @Override
    public void doBusiness(Context mContext) {

    }
}
