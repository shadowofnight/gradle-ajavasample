package com.zftlive.android.library.base.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

/**
 * 对话框基类
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public abstract class BaseDialog extends Dialog {

  /**
   * 弹出对话框的Activity
   */
  protected Activity mActivity;

  public BaseDialog(Activity mActivity){
    super(mActivity);
    this.mActivity = mActivity;
  }

  public BaseDialog(Activity mActivity, int theme) {
    super(mActivity, theme);
    this.mActivity = mActivity;
  }

  @Override
  public void show() {
    if (mActivity.isFinishing() || isDestroyed(mActivity, false)) {
      return;
    }

    super.show();
  }

  /**
   * 复写关闭对话框方法，对外公开回调取消事件
   */
  @Override
  public void cancel() {
    super.cancel();
  }

  /**
   * activity.isDestroyed()的API兼容方法
   * @param mActivity
   * @return API 17以下为defaultValue，其他情况返回a.isDestroyed()的返回结果
   */
  private boolean isDestroyed(Activity mActivity, boolean defaultValue) {
    if (android.os.Build.VERSION.SDK_INT >= 17) {
      return mActivity.isDestroyed();
    }
    return  defaultValue;
  }
}
